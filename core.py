from flask import Flask
from flask_simple_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    app.config['SQLALCHEMY_ECHO'] = True
    app.config['SECRET_KEY'] = 'superSECRET'
    app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
    app.config['SQLALCHEMY_RECORD_QUERIES'] = True
    app.config['SQLALCHEMY_POOL_SIZE'] = 10
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://test_user:test_password@localhost/test_database'

    app.config['TESTING'] = True

    db.init_app(app)

    return app

