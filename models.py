from sqlalchemy import Column, Integer, Text
from core import Base


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(Text, unique=True, index=True)

