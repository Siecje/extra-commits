###Extra Commit mystery

```bash
$ pip install -r requirements.txt
```

```
postgres=# create database "test_database";
CREATE DATABASE
postgres=# create user test_user with password 'test_password';
CREATE ROLE
postgres=# GRANT ALL PRIVILEGES ON DATABASE "test_database" to test_user;
GRANT
postgres=# \q
```

```python
$ py.test -s tests.py
```
