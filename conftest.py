from base64 import b64encode
import pytest
from core import db, Base, create_app
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.engine import create_engine
from models import User

def create_initial():
    db.session.add(User(name='initial'))
    db.session.commit()

@pytest.fixture(scope='session')
def app(request):
    app = create_app()

    ctx = app.app_context()
    ctx.push()

    # def teardown():
    #     ctx.pop()
    #
    # request.addfinalizer(teardown)

    return app

@pytest.fixture(scope='session')
def connection(app, request):

    connection = db.engine.connect()
    transaction = connection.begin()

    Base.metadata.create_all(connection)

    user = User(name='cat2')
    Session = sessionmaker(bind=connection)
    db.session = Session()
    create_initial()
    db.session.add(user)
    db.session.commit()

    def teardown():
        transaction.rollback()

    request.addfinalizer(teardown)
    return connection


@pytest.fixture(scope='function')
def db_session(connection, request):
    ''' Create a new database session for each test. '''
    transaction = connection.begin_nested()
    session_factory = sessionmaker(bind=connection)
    db.session = scoped_session(session_factory)

    def teardown():
        transaction.rollback()

    request.addfinalizer(teardown)
    return db.session


@pytest.fixture(scope='function')
def user(db_session, request):
    return db_session.query(User).filter_by(email='admin@example.com').first()


@pytest.fixture(scope='function')
def client(app, request):
    client = app.test_client()
    client.auth = 'Basic ' + b64encode(
        ('admin@example.com' + ':' + 'admin').encode('utf-8')).decode('utf-8')

