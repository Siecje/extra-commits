from sqlalchemy.engine import create_engine
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.orm.session import sessionmaker


class SQLAlchemy:
    def __init__(self, app=None, bind=None):
        if app is not None:
            self.init_app(app)
        if bind is not None:
            self.engine = bind

    def init_app(self, app):
        def add_kwarg(arg, option):
            if arg in app.config:
                kwargs[arg] = app.config[arg]

        self.app = app

        kwargs = {}
        add_kwarg('pool_size', 'SQLALCHEMY_POOL_SIZE')
        add_kwarg('max_overflow', 'SQLALCHEMY_MAX_OVERFLOW')
        add_kwarg('echo', 'SQLALCHEMY_ECHO')

        self.engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'], **kwargs)
        self.session = scoped_session(sessionmaker(self.engine))
        app.teardown_appcontext(self.close_session)

    def close_session(self, exc):
        try:
            if self.app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] and exc is None:
                self.session.commit()
        finally:
            self.session.remove()

        return exc
